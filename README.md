# UserStyles

Repro for my user stylesheets as found at https://userstyles.world/user/minecraftchest1.

Use this repo to file issues with my stylesheets.

If you do not have a github account, you may also send an email to contact-project+minecraftchest1-userstyles-31734951-issue-@incoming.gitlab.com to open an issue. More info at https://docs.gitlab.com/ee/user/project/service_desk.html#using-service-desk
